/*#-#-#-#-#( Import needed libraries )#-#-#-#-#*/
    #include <Wire.h>
    #include "U8glib.h" // needed for the 12864 LCD => https://github.com/olikraus/u8glib
    #include <OneWire.h> //needed for the DS18B20 => shipped with the Arduino IDE
    #include <DallasTemperature.h> //needed for the DS18B20 => https://www.arduinolibraries.info/libraries/dallas-temperature
    #include <SPI.h> //needed for the communction througth SPI => shipped with the Arduino IDE
    #include <Adafruit_Sensor.h> // needed for the DHT22
    #include <DHT.h> // needed for the DHT22
    #include <DHT_U.h> // needed for the DHT22
/*#-#-#-#-#( Declare Constants and Pin Numbers )#-#-#-#-#*/
    #define photocellPin A0
    #define ONE_WIRE_BUS 6   
    #define bfs_Pin A1 
    #define ccMoisturePin 2
    #define ccTempPin 3
    #define DHTPIN 7 //DHT Data an Pin 7
/*#-#-#-#-#( Declare objects )#-#-#-#-#*/
    OneWire oneWire(ONE_WIRE_BUS);
    DallasTemperature dbSensor(&oneWire);
    U8GLIB_ST7920_128X64_1X u8g(18, 16, 17; // normal (13,11,12)                                  
    #define DHTTYPE DHT22 
    DHT dht(DHTPIN, DHTTYPE);
/*#-#-#-#-#( Declare Variables )#-#-#-#-#*/
    // Measured values
    float temp=0.0; //Value for the temperatur from the DHT22
    float hum= 0.0; //Value for the humanity from the DHT22
    int pcValue =0; //Value for the Photocell
    float tempDB = 0.0; //Value for the DS18B20
    int bfs_Value = 0; // Value for the Moisture Sensor
    int bfs_Value_mapped = 0; // Because the Moisture Sensor return a value between 0 to 1023 (10 Bit), we convert it into a value between 0 to 100
/*#-#-#-#-#( Functions )#-#-#-#-#*/
//function the write data to the LCD
void draw(void) {
 u8g.setFont(u8g_font_04b_24); 
 
     u8g.drawStr( 0, 6,"FlowerMeter");
    
     u8g.drawStr( 0, 10, "-------------------------------------------------------");

     u8g.drawStr(70, 14,"measured:");
     
     u8g.drawStr( 0, 18, "-------------------------------------------------------");
     
     u8g.drawStr( 0, 22, "Temp: (C)");
     u8g.setPrintPos(70,22);
     u8g.print(temp);
     
     u8g.drawStr( 0, 26, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 30, "Humanity: (%)");
     u8g.setPrintPos(70 ,30);
     u8g.print(hum);

     u8g.drawStr( 0, 34, "-----------------------------------------------------------");

     u8g.drawStr( 0, 38, "Light:");
     u8g.setPrintPos(70 ,38);
     u8g.print(pcValue);
     
     u8g.drawStr( 0, 42, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 46, "Moisture: (%) ");
     u8g.setPrintPos(70 ,46);
        if (digitalRead(ccMoisturePin) == 1){ //If the circuit closed (e.g digitalRead() gives back 1), the Moisture Sensor is connect to the board and the measured can be displayed 
        u8g.print(bfs_Value_mapped);
        }
     else{
        u8g.print("n.c");
     }
    
     u8g.drawStr( 0, 50, "-----------------------------------------------------------");

     u8g.drawStr( 0, 54, "Temp ext.: (C)");
     u8g.setPrintPos(70 ,54);
     if (digitalRead(ccTempPin) == 1){ //If the circuit closed (e.g digitalRead() gives back 1), the Moisture Sensor is connect to the board and the measured can be displayed 
        u8g.print(tempDB);
      }
     else {
      u8g.print("n.C");
     }
   
     u8g.drawStr( 0, 58, "-----------------------------------------------------------");  
}
void setup() {
    //PinMode
    pinMode(photocellPin, INPUT); // Set this pin in the Input Mode
    pinMode(ONE_WIRE_BUS, INPUT);   // Set this pin in the Input Mode
    pinMode(ccMoisturePin, INPUT); // Set this pin in the Input Mode
    pinMode(ccTempPin,INPUT);     // Set this pin in the Input Mode
 
    Serial.begin(9600); //Start the Serial Communication with 9600 Baud
    Serial.println("Hello");
    Serial.println("I'm FlowerMeter");
    
    SPI.begin(); // Init SPI bus

     if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255);     // white
     }
    else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
      u8g.setColorIndex(3);         // max intensity
    }
    else if ( u8g.getMode() == U8G_MODE_BW ) {
      u8g.setColorIndex(1);         // pixel on
    }
    else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
      u8g.setHiColorByRGB(255,255,255);
    }

    dht.begin(); //Start DHT22
    
    dbSensor.begin(); //Start the DB18B20 Sensor */
}
 
void loop() {

    //Write data to the LCD
    u8g.firstPage(); 
     do{
        draw();
      } while( u8g.nextPage() );

    //Read sensor
    // DHT22
      hum = dht.readHumidity();                           
      temp = dht.readTemperature(); 
   //Photocell
      pcValue=analogRead(photocellPin); // Read value from the photocell
      pcValue= map(pcValue, 0, 1023, 0, 100); //Mapped the value (0 to 1023) to a better understandable format (0 to 100)
    // DB18B20
       dbSensor.requestTemperatures();
       tempDB=dbSensor.getTempCByIndex(0); // Read the values from the DS18B20
    // Moiture Sensor
      bfs_Value = analogRead(bfs_Pin); // Read the value from the Moisture Sensor
      bfs_Value_mapped = map(bfs_Value, 0, 560, 0, 100);

    /* Debug section  
     * Uncomment this and you can see the measured and readed values on the serial Monitor (9600 Baud)
     *  This is often helpful for troubleshooting
    */
/*
     Serial.println("DHT22 temperature: ");
     Serial.println(temp);
     Serial.println("DHT22 humanity: ");
     Serial.println(hum);*/
     Serial.println("Photocell: ");
     Serial.println(pcValue);
     /*Serial.println("DS18B20 temperature: ");
     Serial.println(tempDB);*/
     Serial.println("Moiture: ");
     Serial.println(bfs_Value);
     Serial.println("Moiture_mapped: ");
     Serial.println(bfs_Value_mapped);
     Serial.println("Status Connection moisture Sensor: ");
     Serial.println(digitalRead(ccMoisturePin));
     Serial.println("Status Connection DS18B20 Sensor: ");
     Serial.println(digitalRead(ccTempPin));
   
    delay(2000);
}
