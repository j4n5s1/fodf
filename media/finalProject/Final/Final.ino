/*   
 *   //SDA = A4
 *   //SCL = A5
*/

/*Pinliste                   |Arduino     |PCB
 * 
 * RFID
 *  RST                      | Pin 9      |
 *  SPI SS(on board as SDA)  | Pin 10     |
 *  SPI MOSI                 | Pin 11     |
 *  SPI MISO                 | Pin 12     |
*  SPI SCK                   | Pin 13       
 *  VCC                      | 3,3V
 *  GND                      | Ground
 *  
 *  
 * Display 
 *  BLK (20)                 |GND
 *  BLA (19)                 |3,3V
 *  PSB (15)                 |GND
 *  E (6)                    |7
 *  R/W (5)                  |6
 *  RS (4)                   |5
 *  V0 (3)                   |5V
 *  VCC (2)                  |5V
 *  GND (1)                  |GND
 * 
 * 
 * 
 * 
 * BME280
 *  SDA                       | SDA A4
 *  SCL                       | SCL A5
 *  VCC                       | 3,3V       
 *  GND                       | Ground
 *  
 *  
 *  Photocell
 *  
 *  DS18B20
 *  
 *  
 *  Switch1 
 *  
 *  Switch2
 *  
 *  
 *  Moisture Sensor
 *  
 */


/*#-#-#-#-#( Import needed libraries )#-#-#-#-#*/

    #include <Wire.h>
    #include "U8glib.h" // needed for the 12864 LCD => https://github.com/olikraus/u8glib
    #include "cactus_io_BME280_I2C.h" // needed for the BME280 // libary: http://cactus.io/hookups/sensors/barometric/bme280/hookup-arduino-to-bme280-barometric-pressure-sensor
    #include <OneWire.h> //needed for the DS18B20 => shipped with the Arduino IDE
    #include <DallasTemperature.h> //needed for the DS18B20 => https://www.arduinolibraries.info/libraries/dallas-temperature
    #include <SPI.h> //needed for the communction througth SPI => shipped with the Arduino IDE
    #include <MFRC522.h> //needed for the RFID Reader => https://github.com/miguelbalboa/rfid
    
/*#-#-#-#-#( Declare Constants and Pin Numbers )#-#-#-#-#*/

    #define photocellPin A0 
    #define db18b20Pin 2 
    #define bfs_Pin A1 
    #define SS_PIN 10
    #define RST_PIN 9
    #define ccMoisturePin 3
    #define ccTempPin 4
    
/*#-#-#-#-#( Declare objects )#-#-#-#-#*/

    // Create BME280 object
    BME280_I2C bme(0x76);
      // The Sensor will use 0x77 as adress for I2C. If this adress is blocked,
      // use intead this and the the adress is 0x76 => BME280_I2C bme(0x76);
    OneWire oneWire(db18b20Pin);
    DallasTemperature dbSensor(&oneWire);
    U8GLIB_ST7920_128X64_1X u8g(7,5,6,U8G_PIN_NONE); // normal (13,11,12)
    //MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
    // MFRC522::MIFARE_Key key; 

    
/*#-#-#-#-#( Declare Variables )#-#-#-#-#*/

    // Measured values
    float temp=0; //Value for the temperatur from the BME280
    float pressure=0; //Value for the pressure from the BME280
    float hum=0; //Value for the humanity from the BME280
    int pcValue =0; //Value for the Photocell
    float tempDB = 0.0; //Value for the DS18B20
    int bfs_Value = 0; // Value for the Moisture Sensor
    int bfs_Value_mapped = 0; // Because the Moisture Sensor return a value between 0 to 1023 (10 Bit), we convert it into a value between 0 to 100

    // readed values
    byte rfidTemp; // optimal Value for Temperatur, readed from the RFID-Token
    byte rfidHum; // optimal Value for Humanity, readed from the RFID-Token Block 1
    byte rfidMoisture; // optimal Value for Humanity, readed from the RFID-Token Block 2
    byte rfidPressure; // optimal Value for Pressure, readed from the RFID-Toke Block 3
    String rfid_Hex_Id = "FF FF FF FF";  // Value for the RFID-Token ID Block 4
    String cardStatus= "No Card";  // String for Status of the RFID Token
    byte nuidPICC[4]; //Byte Array for the Values from the RFID-Token

/*#-#-#-#-#( Functions )#-#-#-#-#*/

//function the write data to the LCD
void draw(void) {
 u8g.setFont(u8g_font_04b_24); 
     u8g.drawStr( 0, 6,"CARD-ID:");
     u8g.setPrintPos(28,6);
     u8g.print(rfid_Hex_Id);
     
     u8g.drawStr( 0, 10, "-------------------------------------------------------");

     u8g.setPrintPos(0,14);
     u8g.print(cardStatus);
     u8g.drawStr( 54, 14, "| measured: | optimal: ");
     
     u8g.drawStr( 0, 18, "-------------------------------------------------------");
     
     u8g.drawStr( 0, 22, "Temp: (C)            | ");
     u8g.setPrintPos(60    ,22);
     u8g.print(temp);
     u8g.drawStr(78, 22,"        |");
     u8g.setPrintPos(100,22);
     u8g.print(rfidTemp);
     
     u8g.drawStr( 0, 26, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 30, "Humanity: (%)     | ");
     u8g.setPrintPos(60 ,30);
     u8g.print(hum);
     u8g.drawStr(78, 30,"        |");
     u8g.setPrintPos(100,30);
     u8g.print(rfidHum);

     u8g.drawStr( 0, 34, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 38, "Pressure:          | ");
     u8g.setPrintPos(60 ,38);
     u8g.print(pressure);
     u8g.drawStr(78, 38,"        |");
     u8g.setPrintPos(100,38);
     u8g.print(rfidPressure);

     u8g.drawStr( 0, 42, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 46, "Light:                 | ");
     u8g.setPrintPos(60 ,46);
     u8g.print(pcValue);
     
     u8g.drawStr( 0, 50, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 54, "Moisture: (%)     | ");
     u8g.setPrintPos(60 ,54);
     if (digitalRead(ccMoisturePin) == 1){ //If the circuit closed (e.g digitalRead() gives back 1), the Moisture Sensor is connect to the board and the measured can be displayed 
        u8g.print(bfs_Value_mapped);
     }
     else{
        u8g.print("n.c");
     }
     u8g.drawStr(78, 54,"        |");
     u8g.setPrintPos(100,54);
     u8g.print(rfidMoisture);

     u8g.drawStr( 0, 58, "-----------------------------------------------------------");
     
     u8g.drawStr( 0, 62, "Temp ext.: (C)    | ");
     u8g.setPrintPos(60 ,62);
     if (digitalRead(ccTempPin) == 1){ //If the circuit closed (e.g digitalRead() gives back 1), the Moisture Sensor is connect to the board and the measured can be displayed 
        u8g.print(tempDB);
     }
     else {
      u8g.print("n.C");
     }
     
}
void setup() {
    //PinMode
    pinMode(photocellPin, INPUT); // Set this pin in the Input Mode
    pinMode(db18b20Pin, INPUT);   // Set this pin in the Input Mode
    pinMode(ccMoisturePin, INPUT); // Set this pin in the Input Mode
    pinMode(ccTempPin,INPUT);     // Set this pin in the Input Mode
 
    Serial.begin(9600); //Start the Serial Communication with 9600 Baud
    Serial.println("Hello");
    Serial.println("I'm FlowerMeter");
    
    SPI.begin(); // Init SPI bus
    //rfid.PCD_Init(); // Init Instance of the  MFRC522 

    /*
    for (byte i = 0; i < 6; i++) {
      key.keyByte[i] = 0xFF;
    }*/

     if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255);     // white
     }
    else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
      u8g.setColorIndex(3);         // max intensity
    }
    else if ( u8g.getMode() == U8G_MODE_BW ) {
      u8g.setColorIndex(1);         // pixel on
    }
    else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
      u8g.setHiColorByRGB(255,255,255);
    }

    //Checking if the BME280 is connect to the board
   /* if (!bme.begin()) {
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }*/
    
    //bme.setTempCal(-1);// Uncomment if you need to set a offset for the temp.
    
    //dbSensor.begin(); //Start the DB18B20 Sensor
}
 
void loop() {

    //Write data to the LCD
    u8g.firstPage(); 
     do{
        draw();
      } while( u8g.nextPage() );

    //Read sensor
      
    //BME280
   /* bme.readSensor(); //Request data from the sensor
    temp=bme.getTemperature_C(); //Read temperatur in °C from the BME280
    pressure= bme.getPressure_MB(); // Read pressure in mBar from the BME280
    hum=bme.getHumidity(); // Read humidity from the BME280
*/
    //Photocell
      pcValue=analogRead(photocellPin); // Read value from the photocell
      pcValue= map(pcValue, 0, 1023, 0, 100); //Mapped the value (0 to 1023) to a better understandable format (0 to 100)

    // DB18B10
    tempDB=dbSensor.getTempCByIndex(0); // Read the values from the DS18B20
    
    // Moiture Sensor
      bfs_Value = analogRead(bfs_Pin); // Read the value from the Moisture Sensor
      bfs_Value_mapped = map(bfs_Value, 0, 560, 100, 0); //To Do => Messen, welcher Wert im Trockenen zustand anzeigt, dann abkleben und ins Wasser halten und notieren. Dann die beiden Werte in den Map Befehl eintragen.
      
/*
    //Check if a new RFID Card present
    if (rfid.uid.uidByte[0] != nuidPICC[0] || 
      rfid.uid.uidByte[1] != nuidPICC[1] || 
      rfid.uid.uidByte[2] != nuidPICC[2] || 
      rfid.uid.uidByte[3] != nuidPICC[3] ) {

    // Store the NUID into the nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
    
    rfidTemp=rfid.uid.uidByte[0]; // Read the optimal value for the temperatur from the RFID Card. Value is stored at position 0 in the array
    rfidHum=rfid.uid.uidByte[1]; // Read the optimal value for the Huminity from the RFID Card. Value is stored at position 1 in the array
    rfidPressure=rfid.uid.uidByte[2]; // Read the optimal value for the Pressure from the RFID Card. Value is stored at position 2 in the array
    rfidMoisture=rfid.uid.uidByte[3]; // Read the optimal value for the Moiture from the RFID Card. Value is stored at position 3 in the array
    cardStatus="Card readed"; // Set the Card Status to "Card readed"
  
    }
    // Halt PICC
    rfid.PICC_HaltA();

   // Stop encryption on PCD
   rfid.PCD_StopCrypto1();
*/

    //Check if theMoiture Sensor is connect to the board. If disconnected, "n.c" will appear on the screen
    



    //Check if the DS18B20S is connect to the board. If disconnected, "n.c" will appear on the screen


    /* Debug section  
     * Uncomment this and you can see the measured and readed values on the serial Monitor (9600 Baud)
     *  This is often helpful for troubleshooting
    */
   
/*
     Serial.println("BME280 temperature: ");
     Serial.println(temp);
     Serial.println("BME280 humanity: ");
     Serial.println(hum);
     Serial.println("BME280 pressure: ");
     Serial.println(pressure);*/
     Serial.println("Photocell: ");
     Serial.println(pcValue);
     Serial.println("DS18B20 temperature: ");
     Serial.println(tempDB);
     Serial.println("Moiture: ");
     Serial.println(bfs_Value);
     Serial.println("Moiture_mapped: ");
     Serial.println(bfs_Value_mapped);
    /* Serial.println("RFID ID Block 1: ");
     Serial.println(rfidTemp);
     Serial.println("RFID ID Block 2: ");
     Serial.println(rfidHum);
     Serial.println("RFID ID Block 3: ");
     Serial.println(rfidMoisture);
     Serial.println("RFID ID Block 4: ");
     Serial.println(rfidPressure);
     Serial.println("Status Connection moisture Sensor: ");
     Serial.println(digitalRead(ccMoisturePin));
     Serial.println("Status Connection DS18B20 Sensor: ");
     Serial.println(ccTempPin);
     Serial.println("Status Card ");
     Serial.println(cardStatus);
     */
    delay(3000);
}

/*To DO 
 * Photocell => Text oder Wert, mappen, funkction entfernen
 * DS18B20 Fixxen, Kabel und Adapter erstellen
 * BME280 Fix 5V auf 3.3V
 * BFS => Ausmessen und dann mappen
 * RFID einbinden
 * Gucken ob Verbunden sonst senden Fehler.
*/


//http://www.mauroalfieri.it/elettronica/reprap-full-graphic-controller-with-arduino.html
