/*   
 *   //SDA = A4
 *   //SCL = A5
*/

/*Pinliste                   |Arduino     |PCB
 * 
 * RFID
 *  RST                      | Pin 9      |
 *  SPI SS(on board as SDA)  | Pin 10     |
 *  SPI MOSI                 | Pin 11     |
 *  SPI MISO                 | Pin 12     |
*  SPI SCK                   | Pin 13       
 *  VCC                      | 3,3V
 *  GND                      | Ground
 *  
 *  
 * Display 
 *  BLK (20)                 |GND
 *  BLA (19)                 |3,3V
 *  PSB (15)                 |GND
 *  E (6)                    |7
 *  R/W (5)                  |6
 *  RS (4)                   |5
 *  V0 (3)                   |5V
 *  VCC (2)                  |5V
 *  GND (1)                  |GND
 * 
 * 
 * 
 * 
 * BME280
 *  SDA                       | SDA A4
 *  SCL                       | SCL A5
 *  VCC                       | 3,3V       
 *  GND                       | Ground
 *  
 *  
 *  Photocell
 *  
 *  DS18B20
 *  
 *  
 *  Switch1 
 *  
 *  Switch2
 *  
 *  
 *  Moisture Sensor
 *  
 */

