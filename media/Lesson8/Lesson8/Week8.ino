/*#-#-#-#-#( Import needed libraries )#-#-#-#-#*/
    #include &lt;Wire.h&gt;
    #include "cactus_io_BME280_I2C.h"
    // libary: http://cactus.io/hookups/sensors/barometric/bme280/hookup-arduino-to-bme280-barometric-pressure-sensor
    #include &lt;OneWire.h&gt;
    #include &lt;DallasTemperature.h&gt;
/*#-#-#-#-#( Declare Constants and Pin Numbers )#-#-#-#-#*/
    #define photocellPin PC0 // The pin with the intern name PC0/ADC0 get now the name bphotocellPin/ Pinnr 23
    #define db18b20Pin PD2 // The pin with the intern name PD2 get now the name ds18b20Pin / Pinnr 2
/*#-#-#-#-#( Declare objects )#-#-#-#-#*/
    // Create BME280 object
    BME280_I2C bme(0x76);
    // The Sensor will use 0x77 as adress for I2C. If this adress is blocked,
    // use intead this and the the adress is 0x76 => BME280_I2C bme(0x76);
    OneWire oneWire(db18b20Pin);
    DallasTemperature dbSensor(&oneWire);
/*#-#-#-#-#( Declare Variables )#-#-#-#-#*/
    int pcValue =0; //Value for the Photocell
    float dbValue = 0.0; //Value for the DS18B20

//Functions
String pcRead() {
    String returnString;
    pcValue=0;
    pcValue=analogRead(photocellPin);

    if (pcValue < 205){
        returnString = "Dark ";
        returnString += pcValue;
        return returnString;
    }
    if (pcValue < 410) {
        returnString = "Not so dark ";
        returnString += pcValue;
        return returnString;
    }
    if (pcValue <615) {
        returnString = "In the Middel ";
        returnString += pcValue;
        returnString;
    }
    if (pcValue <820) {
        returnString = "Not so bright ";
        returnString += pcValue;
        return returnString;
    }
    else {
        returnString = "Bright ";
        returnString += pcValue;
        return returnString;
    }
}
void setup() {
    //PinMode
    pinMode(photocellPin, INPUT);
    pinMode(db18b20Pin, INPUT);

    Serial.begin(9600); //Serial Communication with 9600 Baud
    Serial.println("Hello");
    Serial.println("I'm FlowerMeter");

    if (!bme.begin()) {
        Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
    }
    //bme.setTempCal(-1);// Uncomment if you need to set a offset for the temp.
    dbSensor.begin(); //Start the DB18B20 Sensor
}

void loop() {

    //Photocell
    Serial.println(pcRead());

    //BME280
    bme.readSensor();
    Serial.println("BME280");
    Serial.print(bme.getPressure_MB());
    Serial.println("mb");
    Serial.print(bme.getHumidity());
    Serial.println("%");
    Serial.print(bme.getTemperature_C());
    Serial.println("*C");

    //DS18B20
    dbSensor.requestTemperatures();
    Serial.print("DB18B20 Temp: ");
    Serial.println(dbSensor.getTempCByIndex(0));

    delay(6000);
}
