/*#-#-#-#-#( Import needed libraries )#-#-#-#-#*/
 
/*#-#-#-#-#( Declare Constants and Pin Numbers )#-#-#-#-#*/
#define ledPin PB0
#define boardButton PB1
 
/*#-#-#-#-#( Declare objects )#-#-#-#-#*/
 
/*#-#-#-#-#( Declare Variables )#-#-#-#-#*/
int waitingTime = 1000;
int shortTime = 2000;
int longTime = 3000;
boolean buttonState = false;
 
//Functions
void ON (){
  // --- -.
  // --- = O
  digitalWrite(ledPin, LOW);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  // -. = N
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
  }
 
void OFF(){
  // --- ..-. ..-.
  // --- = O
 
  digitalWrite(ledPin, LOW);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  // ..-. = F
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  // ..-. = F
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(longTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
 
  digitalWrite(ledPin, HIGH);
  delay(shortTime);
  digitalWrite(ledPin, LOW);
  delay(waitingTime);
}
 
void LedBlink (int amount)
{
  for(int i; i<=amount;i++){
    digitalWrite(ledPin, HIGH);
    delay(waitingTime);
    digitalWrite(ledPin, LOW);
    delay(waitingTime);
  }
}
 
void setup() {
//pinMode setzen
    pinMode(ledPin, OUTPUT);
    pinMode(boardButton, INPUT);
 
//Serial Communication start
  Serial.begin(9600);
  Serial.print("Hello");
}
 
void loop() {
 if (boardButton == HIGH && buttonState == false){
    ON();
    buttonState = true;
    Serial.println("I'm listen to you");
    Serial.println("Enter a number and the LED will blink for you");
    do{
      char readValue =Serial.read();
      LedBlink(readValue);
      if(boardButton == LOW){
        buttonState = false;
        OFF();
        Serial.println("Now I'm not listening to you anymore.");
      }
    }
    while(buttonState == true);
  }
}
